# -*- coding: utf-8 -*-
"""
@file       ME405_Lab3.py
@brief      A program to collect ADC data observing the step response of 
            button B2
@details    This file waits for the PC to send 'G' over the serial connection.
             Upon receiving the signal, it runs the ADC constantly at 100 kHz,
             filling up a 5000 pt buffer with voltage data from the button
             pin. Upon detecting the button has been pressed, it stops
             collecting data and sends the buffer to the PC.
            
@author     Craig Belshe
@date       May 6, 2021
"""

import pyb, array

uart = pyb.UART(2)
SwitchA = pyb.Switch()

pin = pyb.Pin.board.PA0
adc = pyb.ADC(pin)
LED1 = pyb.LED(1)
LED1.on()

buffy = array.array('H', (0 for index in range(5000)))

while True:
    
    # Wait to receive 'G' from the PC 
    if uart.any() != 0:
        val = uart.readchar()
        if val == 71:
            
            # Begin measuring with ADC and write to buffy
            while True:
                adc.read_timed(buffy, 100000)
                
                # Stop measuring once respose detected
                if buffy[-1] - buffy[0] > 3500:
                    LED1.off()
                    
                    # send each data point from buffer to PC
                    for item in buffy:
                        uart.write('{}\r\n'.format(item))
                    break
            break
            