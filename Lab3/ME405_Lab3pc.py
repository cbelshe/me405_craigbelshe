# -*- coding: utf-8 -*-
"""
@file       ME405_Lab3pc.py
@brief      The front end program to receive ADC data from the Nucleo
@details    Sends a go message to the nucleo through the serial connection 
            on COM3, then waits for a response. Assumes the Nucleo will
            send 5000 lines containing the Voltage values from 0 to 4095,
            and that the ADC collected data at 100 kHz. Truncates the data
            to better focus on the initial response, then plots the data.
            Saves the plot and data as Plot.png and datafile.csv,
            respectively, in the program's folder.
            
@author     Craig Belshe
@date       May 6, 2021
"""

import serial, array, csv
from matplotlib import pyplot

# Set up serial connection
ser = serial.Serial(port='COM3', baudrate=115200, timeout=1)
data = array.array('f')
#time = array.array('f')

# send Go command to Nucleo
while True:
    ch = input('Enter Letter: ')
    ser.write(str(ch).encode())
    if ch == 'G':
        break

# Wait until Nucleo responds
while (ser.in_waiting==0):
    pass

# Receive each line of data and append to data array
for n in range(5000):
    line = ser.readline().decode()
    #print(line)
    data.append(float(line.strip()))
    #time.append(float(n*.00001))

ser.close()

# Below for modifying data to remove points before the actual response and 
# points more than 50 after it
loc1 = 0
cnt2 = 0
loc2 = 0
for i in range(len(data)):
    if (data[i] > 4080):
        cnt2 += 1
    if cnt2 == 50:
        loc2 = i
    if data[i] < 25:
        loc1 += 1
if loc2 == 0:
    loc2 = 5000
    
data_trimmed = array.array('f')
time_trimmed = array.array('f')
for i in range(len(data)):
    if i < loc2:
        if i > loc1:
            data_trimmed.append(data[i]*3.3/4095*1000)
            time_trimmed.append(float((i-loc1)*.01))
    

# Plot data vs time
pyplot.figure()
pyplot.plot(time_trimmed, data_trimmed)
pyplot.xlabel('Time (ms)')
pyplot.ylabel('Voltage (mV)')
pyplot.title('Step Response of B2')
pyplot.yticks([0,500,1000,1500,2000,2500,3000,3300])

# Save plot as Plot.png
pyplot.savefig('Plot.png')

# Write data and time to datafile.csv file
with open('datafile.csv', 'w', encoding='UTF8', newline='') as file:
    w = csv.writer(file)
    w.writerow(['Time (ms)', 'Data (mV)'])
    for n in range(len(data_trimmed)):
        #w.writerow('{time}, {data}'.format(time=str(time_trimmed[n]),
        #                                   data=str(data_trimmed[n])))
        w.writerow([str(time_trimmed[n]), str(data_trimmed[n])])
