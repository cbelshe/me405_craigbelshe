##
#
# @file                mainpage.py
# @brief               Brief doc for mainpage.py
# @details             Detailed doc for mainpage.py
#
# @mainpage
#
# This is my portfolio for ME 405: Mechatronics. Four separate labs are documented here, as well as the Term Project.
#
# @section sec_port    Portfolio Details
#
#
# Included Modules:
#  * Term Project (\ref FinalProject)
#  * Lab 0x01 (\ref sec_lab1)
#  * Lab 0x02 (\ref sec_lab2)
#  * Lab 0x03 (\ref sec_lab3)
#  * Lab 0x04 (\ref sec_lab4)
#  * HW 0x02 (\ref HW2)
#  * HW 0x04 (\ref HW4)
#  * HW 0x05 (\ref HW5)
#
#
# @section FinalProject ME405: Term Project
#    * Source: https://bitbucket.org/cbelshe/me405_craigbelshe/src/master/TermProject/
#    * Documentation: \ref TermProject_details
#
# This is the final project for ME 405. The purpose is to balance a ball on top of a platform which can be tilted with
# two motors. Two sensors were used to keep track of the system - encoders to determine the position of each motor,
# and a touch panel on the platform to track the balls's position.
#
#
#
# @section sec_lab1   Lab 0x01 Documentation
#   * Source: https://bitbucket.org/cbelshe/me405_craigbelshe/src/master/Lab1/
#   * Documentation: \ref Lab1
#
# A basic assignment to practice implementing FSMs in Python using generators. Implements a theoretical Vending Machine
# with multiple states (Idle, receiving money, dispensing drink, etc).
#
# @section sec_lab2   Lab 0x02 Documentation
#    * Source: https://bitbucket.org/cbelshe/me405_craigbelshe/src/master/Lab2/
#    * Documentation:
#        * \ref ME405_Lab2_partA.py
#        * \ref ME405_Lab2_partB.py
#
# Two different implementations for a basic reaction game, which asks the user to press a button quickly after a LED
# lights up to determine their reaction time. The first implementation (\ref ME405_Lab2_partA.py) uses an interrupt
# tied to the button and a timing function from the utime library to record the reaction time. The second
# implementation (\ref ME405_Lab2_partB.py) uses the built in timer hardware to get a more accurate time result. Both
# continue to record scores until the user presses Ctrl-C, and then print the Average score to console and exit.
#
# @section sec_lab3 Lab 0x03 Documentation
#    * Source: https://bitbucket.org/cbelshe/me405_craigbelshe/src/master/Lab3/
#    * Documentation: \ref Lab3
#    * Data: \ref Lab3data
#
# The purpose of this lab was to create a plot of the step response of the button attached to the board. The data was
# taken using the ADC, and then sent to a separate program on the the PC for processing.
#
# @section sec_lab4 Lab 0x04 Documentation
#    * Source: https://bitbucket.org/cbelshe/me405_craigbelshe/src/master/Lab4/
#    * Documentation: \ref Lab4
#    * Data: \ref Lab4data
#
# Created a class to interface with an external temperature sensor using I2C (\ref mcp9808.py). Then recorded the data
# along with the internal temperature of the board for a period of time. This data was recorded in a .csv file, and
# then graphed.
#
# @section sec_hw2    HW 0x02 Results
#    * \ref HW2
#
# Created a model for the system to predict the movement of the ball on the platform.
#
# @section sec_hw4 HW 0x04 Results
#    * \ref HW4
#
# Implemented a state-space model in Simulink to ensure the system model behaved as expected.
#
# @section sec_hw5 HW 0x05 Results
#    * \ref HW5
#
# Determined values to use for the K constants from the model. This was done by choosing pole locations and working
# from there.
#
# @author              Craig Belshe
# @date                June 10, 2021
#
# @page HW2 Homework 2
# HW 2: System Modelling
#
# I completed this assignment with Daniel Freeman, and our work is shown below. In this assignment we determined a
# system model for the movement of the ball and platform.
#
# \htmlonly
#
# <iframe src="https://drive.google.com/file/d/1yDaWi4dIAEpyu0nz771xbj5ttAVrSP5H/preview"
# width="1088" height="816"></iframe>
#
# \endhtmlonly
#
# @author              Craig Belshe
# @date                June 10, 2021
#
# \page HW4 Homework 4
# The results for Homework 4 are shown below. I worked with Daniel Freeman
# for this assignment. I choose to simply record everything in a document
# and attached it below, for easier viewing.
# \n
# \htmlonly
# <iframe src="https://docs.google.com/document/d/e/2PACX-1vQhPl8OE-0hrAjU25XDuRcQnYa3gGylx_deyDTMdB_eGZPkYsULSZaTwSIXlwxcROhmL3XigNHmbGU7/pub?embedded=true"width="800" height="600"></iframe>
# \endhtmlonly
#
# @author              Craig Belshe
# @date                June 10, 2021
#
# \page HW5 Homework 5
#
# \section section_1 Calculations
#
# After completing the system model in \ref HW4, I choose the closed
# loop pole locations by changing the constant K = [k1 k2 k3 k4].
# Hand calculations for new K values are shown below. 3 different sets of
# values were computed and compared based off settling time and overshoot.
#
# \n
# \htmlonly
# <iframe src="https://drive.google.com/file/d/159-BLpQJApQ6_98iADegNAPq10Yo_Skv/preview" width="800" height="600"></iframe>
# \endhtmlonly
# \n
# \section section_2 Simulation
#
# The goal of finding these K values was to ensure the system could quickly
# the ball and reach equilibrium. Specifically, I aimed to reach steady-state
# within 0.5 seconds and not go further than around 10 cm from center.
# \n
# \n
# To test each set of K values, the Simulink model from HW4 was run,
# but with the ball starting with a displacement of 10 cm. This large
# starting distance was chosen so that the response would be more obvious.
# For each of 3 different iterations, the graphs of position, angle, velocity,
# and angular velocity are shown below:
# \n
# \htmlonly
# <p>With &#950=0.9 and &#969<sub>n</sub> = 6&#183&#960:</p>
# <br>
# <iframe src="image2.png"width="1624" height="916"></iframe>
# <br>
# <p>The System is reaching equilibrium fairly quickly - a little over 0.5 s.
# However, there is a fair bit of overshoot occurring and I thought the ball
# velocity and angle velocity got rather high.</p>
# <br>
# <p>With &#950=0.5 and &#969<sub>n</sub> = 2&#183&#960:</p>
# <br>
# <iframe src="image1.png"width="1601" height="910"></iframe>
# <br>
# <p>The system took well over a second to stabilize - way too long. However,
# it was quite a bit more smooth, with neither velocity or angular velocity
# getting super high.</p>
# <p>With &#950=0.4 and &#969<sub>n</sub> = 10&#183&#960:</p>
# <br>
# <iframe src="image3.png"width="1612" height="902"></iframe>
# <br>
# <p>Since realistically, starting at 10 cm is not likely, I also ran this
# simulation with a starting displacement of 5 cm. When started at 5cm,
# the ball never goes more than 10 cm from the center:</p>
# <br>
# <iframe src="image4.png"width="1583" height="915"></iframe>
# <br>
# \endhtmlonly
# \n
# The third iteration seems to best fit the criteria needed. The resulting K values are:
# \n
# \n
#      <b>K = [-150.38 -11.47 -14.84 -0.794].</b>
# \n
# \n
# Overall, when compared to the K values used in \ref HW4, the settling time
# has been cut down from about 15 s to about 0.4 s. However, the initial
# overshoot has increased drastically - if the ball is right on the edge
# it may fall off.
#
# @author              Craig Belshe
# @date                June 10, 2021
#
# @page Lab3data Lab 3 Data
# In Lab 3, the Step Response of the button B2 was measured using the builtin ADC.
#
# \section sectionP Program Outputs
#
# \subsection data Raw Data
# The generated data (datafile.csv) is shown below:
# \n
#
# \htmlonly
# <iframe src="https://drive.google.com/file/d/1UYhAu4npgKv1mfruT15BLYbXFXhKSfYh/preview"
# width="640" height="480"></iframe>
# \endhtmlonly
# \n
#
# \subsection plot Data Plot
# The generated pyplot is shown below:
# \n
#
# \htmlonly
# <iframe src="https://drive.google.com/file/d/1RPXfnl9bhIy4RZw4wNCqBwAHDMnETpUc/preview"
# width="640" height="480"></iframe>
# \endhtmlonly
# \n
#
# \section section2 Time Constant
# \subsection theory Theoretical Calculated Value for Time Constant
# \htmlonly
# <p>
# We know Capacitance C = 100 nF, and Resistance R = 4.8 k&#937
# <br>
# Time Constant = &#964 = R*C = (4.8k)*(100n) = 480 &#956s
# </p>
# \endhtmlonly
# \n
# \subsection datat Time Constant from Measured Data
# From class we know the time constant should occur around the time that the
# voltage measured is about 0.632 times the maximum (or 63.2% of the max value).
# \n
# The maximum voltage that was measured by the ADC was about 3290 mV,
# 0.632*3290 = 2079 mV, so the time constant is roughly equal to the
# time where the measured voltage is closest to 2079 mV. From the
# datafile.csv, the measured data comes closest at 2074 mV, which occurs
# at time t = 0.48 ms, or 480 us.
# \n
# \n
# Therefore, the measured data supports the conclusion that the time
# constant is 480 us, since the calculated and measured values were both 480 us
# \n
#
# @author              Craig Belshe
# @date                June 10, 2021
#
# @page  Lab4data Lab 4 Data
# In this lab, methods were created to measure the ambient and internal
# temperatures. These measurements were then done every minute to create a .csv file
# containing all measurements for a period of time. Unfortunately, I
# continued to have some form of error that occurred after running for ~5-10
# minutes, where the board would stop recording and exit the program without
# a message. For this reason, Data was taken every 10 s for about 8.5 minutes
# (51 data points). This data was then plotted using the plot.py file. The
# resulting figure is shown below.
# \n
# \htmlonly
# <iframe src="https://drive.google.com/file/d/1VTZslfT8_nIJj5vVnOaFBiT0AjHBhI46/preview" width="640" height="480"></iframe>
# \endhtmlonly
# \n
# Due to the short period of time, the ambient temperature (measured indoors)
# remained relatively steady at ~23 degrees C, or 73 degrees Fahrenheit.
# Since my room did not change temperature much over 8 minutes, this is
# sensible. The Internal temperature of the board varied a lot more,
# and was consistently higher than the ambient temperaturee, at ~30
# degrees Celsius. This makes sense since the internal temperature
# sensor is less accurate, and since the teperature of the board would
# be affected by the boards operation. To test the ambient sensor to
# insure it worked when the temperature was more varied, I tried sticking
# it inside a box taken from my freezer. This allowed it to decrease by as
# much as 10 degrees C, but did not create good plottable data since it
# quickly returned to room temperature.
#
# @author              Craig Belshe
# @date                June 10, 2021
#
# @page Touch_Panel_Details TouchPanel Details
#
# The Touch Panel Sensor's purpose is to determine the x and y position of the ball on the platform. By controlling
# four pins (Two in the "X" direction and two in the "Y" direction), the distance in mm from the origin (the center
# of the panel) can be found. There are 3 primary functions to the Touch Panel Driver:
#    * scan_X(): Returns the X coordinate in mm
#    * scan_Y(): Returns the Y coordinate in mm
#    * scan_Z(): Returns True if ball (or another object) is in contact with the panel, else returns False
#
# The scan_ALL() function can be used to conveniently perform all 3 measurements. More documentation can be found at
# \ref TouchPanel.py.
# \n
# \n
# The Resistive Touch Panel works by having two wires that wind back and forth from on side to the other side of the
# panel. One wire's resistance only increases by a noticeable amount in the X direction, while the other only in the Y
# direction. If you were to look at a point 2/3 of the way through the panel, then 2/3 of the resistance of the wire
# is before that point, and 1/3 after. When the touch panel is touched, the wires short together at that point. This
# allows the position of the touch to be measured. Each direction is measured individually by setting one side of the
# wire for that direction to 3.3 V, and the other to ground. The wire that is not being measured is connected to an ADC
# on one side while the other side is allowed to float. This allows the voltage at the intersection to be measured,
# which can be used to determine the position in that direction thanks to the linear increase of the wires resistance.
# To determine if something is touching the panel at all, one wire has one end connected to 3.3 V, while the other has
# one end connected to 0 V. If the other end of the 0 V connected wire is measured to also be 0 V, then the wires
# cannot be connected together, so the panel is not being touched. If the value read is nonzero, then the wires must
# have been connected and the panel was touched.
# \n
#
#
# \htmlonly
# <p>A simplified circuit diagram for the touch panel is shown below. The pins (Yp, Ym, Xp, Xm) in this case are
# configured for scan_Z - determining if the panel is being touched.</p>
# <p align="center"><iframe src="ME405_tp_diagram.png" width="842" height="354"></iframe></p
# <p>When the touch panel is not being touched, the two wires are not
# connected to each other, and in this case Xp is tied to 0 V while Ym is tied to 3.3 V. When the panel is touched at
# point P, the two wires intersect. Since Xp is no longer 0 V and Ym is no longer 3.3 V, we can determine that the
# touch panel is detecting something. To find the position itself, we just find the Voltage at point P - the position
# in each direction will be the Voltage at point P as a percent of the input voltage multiplied by the total length of
# the panel in that direction.</p>
# <br>
# <p>The pin layout for connecting the touch panel to a cable and then to the board is shown below:</p>
# <p align="center"><iframe src="tp_pins.png" width="798" height="313"></iframe></p>
# <br>
# <br>
# <p> A picture of the touch panel on the platform:</p>
# <iframe src="ME405_tp_test2.jpg" width="712" height="382"></iframe>
# <p> Above is an image of the touch panel on the platform, with nothing touching it. This was used to test the scan_Z
# function in the benchmarks.</p>
# <iframe src="ME405_tp_test1.jpg" width="696" height="392"></iframe>
# <p>Above is an image of the touch panel on the platform, with the ball that it senses on top. This is the position
# used to measure the benchmark data.</p>
# <br>
# <p>Below is an image of the output of the functioning TouchPanel in PuTTY, including the scan_ALL and the total time for each
# operation:</p>
# <iframe src="tp_testingim.jpg" width="706" height="530"></iframe>
# \endhtmlonly
#
#
#
# \section Cal Calibration of Touch Panel
# \n
# The Panel dimensions were found to be 192mm x 112mm. However, measurements did not always function on the border, so
# the possible measurement ranges are below:
#    * minimum X: -84 mm
#    * maximum X: +84 mm
#    * minimum Y: -48 mm
#    * maximum Y: +42 mm
#
# The minimum and maximum Y values likely differ since the panel is slightly off center in the Y direction - there is
# an extra couple millimeters of dead space on the negative y side to give room for the wires used to connect it to the
# MCU. As a result, the measured center is slightly above the center of the panel, by about 3 mm. Otherwise, the center
# measurement was found to be spot on - within a millimeter at least, though I lack the measurement tools to determine its
# exact position. However, measurements further out from the center became much less accurate, since the resistance of
# the panel did not seem to be linear.
#
#
# \section Ben Benchmarks for Timing
# \n
#
# Each function was tested by running it 50 times in the benchmark. Timing was done using the ticks_us function from
# the utime library.
#
# \subsection scanz Scan_Z Test:
#
# \htmlonly
# <p>With the ball off of the platform: </p>
# <ul>
#    <li>All 50 results were False, which is correct.</li>
#    <li>The Total benchmark time was 18392&#956s</li>
#    <li>The Average function time was 367.84&#956s</li>
# </ul>
# <p>With the ball on the platform: </p>
# <ul>
#    <li>All 50 results were True, which is correct.</li>
#    <li>The Total benchmark time was 18403&#956s</li>
#    <li>The Average function time was <b>368.06&#956s</b></li>
# </ul>
# \endhtmlonly
#
#
# \subsection scany Scan_Y Test:
#
# \htmlonly
#
# <p>With the ball on the platform at around Y = -28mm: </p>
# <ul>
#    <li>The maximum variation in Y was 0.073 mm</li>
#    <li>The Standard Deviation in Y was 0.02 mm</li>
#    <li>The Total benchmark time was 19712&#956s</li>
#    <li>The Average function time was <b>394.2&#956s</b></li>
# </ul>
# \endhtmlonly
#
# \subsection scanx Scan_X Test:
#
# \htmlonly
#
# <p>With the ball on the platform at around X = 20.7mm: </p>
# <ul>
#    <li>The maximum variation in X was 0.17mm</li>
#    <li>The Standard Deviation in X was 0.038 mm</li>
#    <li>The Total benchmark time was 19568&#956s</li>
#    <li>The Average function time was <b>391.36&#956s</b></li>
# </ul>
# \endhtmlonly
#
# \subsection scanall Scan_ALL Test:
#
# \htmlonly
#
# Testing all three functions together in the scan_ALL() function.
# <ul>
#    <li>The Total benchmark time was 55139&#956s</li>
#    <li>The Average function time was <b>1102.78&#956s</b></li>
# </ul>
# \endhtmlonly
#
# \subsection scansum Summary of Benchmarks
# All three basic scan functions meet the required specs: they all measure under 500 microseconds individually. As a
# result the scan_ALL() function can be performed in about 1.1 ms, which is easily faster than the estimated 1.5ms
# needed for the system to function smoothly. Both X and Y measurements were accurate across many trials, with only
# small variations. The X measurements varied more than the Y measurements most likely because the X axis is longer.
# The scan_Z() function was equally consistent in identifying if the ball was in contact with the touch panel.
# \n
# \n
# @section us Usage of Touch Panel Driver
# To use the touch panel with the driver, initialize an object for the class and assign the correct pins for each input
#    - If yp from the panel is connected to the PA6 pin on the MCU, then use yp=pyb.Pin.board.PA6 when
# initializing.
#
# Then, scan_Z() can be used to check if something is touching the board before finding the position by
# calling scan_X() and scan_Y(). scan_ALL() can be used to conveniently check all three - it will return a tuple with
# (Z, X, Y).
#
# @author              Craig Belshe
# @date                June 10, 2021
#
# @page Motor_Details Motor Details
#
# The Motor Driver allows the DC motors to be driven at different power levels using PWM. The user can specify the
# Duty cycle for each motor. If a fault occurs (such as when something is blocking the motor) the motor will
# automatically be disabled. It can be re-enabled with clear_fault(). The motors can also be enabled or disabled with
# enable() and disable() respectively. Upon initializing the driver, takes the pins of the motor as well as the fault
# and sleep pins. Documentation can be found at \ref Motor.py
#
# Below is an image of the two motors. To set dutycycle, use set_level_1() for Motor 1 and set_level_2() for motor 2.
# Duty cycle is used as a percent not a decimal.
# \htmlonly
# <iframe src="ME405_motors.jpg" width="818" height="613"></iframe>
# \endhtmlonly
#
# Each motor has 2 inputs to control direction and torque. The two motors share the input nSLEEP, which tells the motors
# when not to sleep. They also both share the fault pin, nFAULT, which goes low whenever a fault occurs in one of the mots
# and must be cleared before operation may continue. The affect of the inputs on motor operation is shown below:
# \htmlonly
# <p align="center"><iframe src="motor_io.png" width="1030" height="281"></iframe></p>
# \endhtmlonly
#
# The motors spin when one input is high and the other low. With constant input voltages, the motors behave in an on or
# off manner In order to adjust the power of the motor, Pulse Width Modulation (PWM) was used. This allowed the
# dutycycle of the input wave to be adjusted. At 100% duty cycle, when the wave is just a dc constant, the motors
# operate at full power. By decreasing the percent of time the wave is high compared to low, the power delivered to the
# motors can be decreased.
#
# The motor pins and their corresponding pins on the board are shown below. IN1/IN2 correspond to Motor 1, while IN3/IN4
# correspond to Motor 2. The inputs are connected to a Timer for PWM.
# \htmlonly
# <p align="center"><iframe src="motor_pins.png" width="750" height="362"></iframe></p>
# \endhtmlonly
# \n
#
# @section mdemo Video Demonstration
# Below is a demonstration of motor 1 in action. It is being started at 0% duty cycle and the duty cycle is
# increased by 1% every half second, until it reaches a duty cycle of 100%. At that point a fault is triggered by
# forcefully grabbing onto the motor, and the motor stops. (Note: the motor is grabbed before it reaches 100%, so
# it does not trigger a fault at first).
#
# \htmlonly
# <iframe width="760" height="400" src="https://www.youtube.com/embed/dSEngHIxrGE" title="YouTube video player"
# frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
# allowfullscreen></iframe>
# \endhtmlonly
#
# @section bdc Dutycycle Benchmark
# To test the minimum duty cycle needed for the motors to overcome static friction and begin spinning, each one was set
# to 0% duty cycle, and then duty cycle was increased in intervals of 1% until the motor began moving consistently.
# \n
# \n
# For Motor 1:
#    * At ~20% dutycycle, motor begins inconsistent, stop and go movement.
#    * Direction Counterclockwise: 27% dutycycle needed
#    * Direction Clockwise: 28% dutycycle needed
#
# For Motor 2:
#    * At ~28% dutycycle, motor begins inconsistent, stop and go movement.
#    * Direction Counterclockwise: 33% dutycycle needed
#    * Direction Clockwise: 34% dutycycle needed
#
# @author              Craig Belshe
# @date                June 10, 2021
#
# @page Encoder_Details Encoder Details
# The Encoder Driver is used with the encoders to update the angle / angular velocity of the motors. Like with the
# motors it initializes with the pins that connect to the encoders. The EncoderDriver class includes a method for
# updating the current position of the motor with update(), and a separate method for retrieving the current position (get_position()). There is also
# a method for the change in angle from the previous update (the angular velocity) and one to manually reset the
# current angle to a chosen value. Documentation can be found at \ref Encoder.py.
#
# The encoder driver automatically handles overflow (or underflow) by noting that if the tick count for the encoder has
# changed by over half its possible values, then it most likely has reached overflow, and then compensating by adding or
# subtracting 65535 (the total number of ticks in the encoder measurement).
#
# To test that the encoders were working, the platform was tilted back and forth. The tick counter adjusted with the platform
# and increased in one direction but decreased in the other. From the starting position, it was possible to reach a
# negative position which indicated that the underflow was working correctly. Since overflow works similarly but in the
# opposite direction, it was assumed that overflow would also work fine. The video below demonstrates the usage of one
# of the encoders while tilting the platform. The data can be seen listed in the terminal - the units are ticks, and
# there are around 4000 ticks to a rotation on the encoder. Apologies for the vertical format.
#
# \htmlonly
#
# <video class="tab" controls>Your browser does not support the &lt;video&gt; tag.
#   <source src="Encoder_Test.MOV"/>
# </video>
# \endhtmlonly
#
# \n
# \n
# The Encoder pins are attached as described in the table. The pins they are connected to are connected totimers which
# support encoder functionality. These are necessary since the timing for encoders is too sensitive to do using pure
# software.
#
# \htmlonly
# <p align="center"><iframe src="enc_pins.png" width="556" height="258"></iframe></p>
# \endhtmlonly
#
#
# @page Ctrl_Details Controller Details
# The Controller object uses the input data of position, theta, velocity, and angular velocity to determine the correct
# duty cycle for the motor according to the system model. When instantiating, it takes the K constants from \ref HW5,
# [k1 k2 k3 k4] as arguments. The controller needs to be defined in both the x and y direction - each controller takes
# one set of inputs and controls one motor. The only method in this class is DutyCycle(x, theta, x_dot, theta_dot), which
# returns the correct duty cycle as a percent for the motors. More on usage can be found at \ref Controller.py
#
# The Controller calculates Torque with T = -x*k, or T = (k1*x + k2*theta + k3*velocity + k4*ang_velocity). Using the
# Torque, it finds the duty cycle using the Torque Constant K of the motors (13.8 mNm/A) and the DC resistance
# (2.21 Ohms). The Duty cycle was found using the equation:
#
# \htmlonly
# <p align="center">
# <iframe src="ctrl_eqn.png" width="392" height="106"></iframe><br>
# or, D = 100% * T * (R / (N*V*K))<br>
# with the constants R, N, K, and V:<br>
# <iframe src="ctrl_cons.png" width="782" height="184"></iframe></p>
# \endhtmlonly
#
# Where K' is the vector of K values described above and found in \ref HW5, and the X vector contains the input
# parameters position, angle, velocity, and angular velocity. The constants in from \ref HW5 were adjusted quite a bit
# to fit the system in the real world. This is likely due to a variety of factors including: unideal motors, inaccuracy
# in angle and position measurement, small timing errors leading to small differences in velocity measurements,
# friction, and other factors not taken into account in the theoretical system model.
#
# @page TermProject_details Term Project
#
# The Term Project was completed in pairs - Neil Patel worked with me on this project.
#
# The goal of the term project is to balance a ball on top of a platform. The platform used is shown below:
#
# \htmlonly
# <iframe src="ME405_platform.jpg" width="679" height="487"></iframe>
# \endhtmlonly
#
# The first step of the Term Project was to model the platform and ball. This was done in \ref HW4 and \ref HW5 . The
# next part included getting each of the individual parts working - the Touch Panel on the top, the motors, and the
# encoders. The model of the platform was used to build the controller, which relies on the ball's position data (X,Y)
# from the touch panel and the motor angle data (theta_X, theta_Y) from the encoder, as well as their derivatives
# velocity and angular velocity.
#
# The repository for all Term Project source code can be found at:
#    * https://bitbucket.org/cbelshe/me405_craigbelshe/src/master/TermProject/ \b (Craig)
#    * https://bitbucket.org/npatel68/me405_labs/src/master/labff/ \b (Neil)
#
# Each Segment of the project is broken down below:
#    * Touch Panel: Utilizing the Touch Panel to find the position of the ball on the platform.
#       * \ref Touch_Panel_Details
#    * Motors: Implemented PWM to control the strength of 2 DC motors to tilt the platform.
#       * \ref Motor_Details
#    * Encoders: Using encoders to track the rotation of the motors.
#       * \ref Encoder_Details
#    * Controller: Determines the Torque and corresponding Duty cycle needed to balance the system
#       * \ref Ctrl_Details
#
# The final step to the project was to run all 4 segments together. This required a scheduler to make sure all tasks
# ran cooperatively. Much of this was done through \ref cotask.py which was provided by the instructor for this class.
# To easily share data (such as position and angle measurements), a second provided module called \ref task_share.py was
# used.
#
# @section scheduler Scheduler
# The Scheduler, \ref main.py , defines tasks for each of the above segments (Touch Panel, Motors, Encoders, and the
# Controllers).
#
# The Encoder and Touch Panel tasks are fairly straight forward - they update position and angle
# respectively, and then save it to a shared queue so that the other tasks may access it. The shared queue is provided by
# \ref task_share.py. The Controller Task creates two instances of the Controller class using specified K values, one for
# each direction, and then uses the data from the Touch Panel and Encoders to get a duty cycle, which it also adds to a
# shared queue. The Motor Task then runs and sets the motors to the correct duty cycle. The  tasks are set so that the
# Encoder and Touch Panel tasks run once every 40 ms, and the other two tasks run once every 15 ms.
#
# A list of scheduler tasks and shared queues and their descriptions is shown below:
#
# \htmlonly
# <iframe src = "taskShare.png" width = "634" height = "240" ></iframe>
# \endhtmlonly
#
#
# @section videodemo Video Demonstration
# Neil's working platform is demonstrated below. The video contains a brief explanation of our procedure:
#
# \htmlonly
# <iframe width="500" height="810" src="https://www.youtube.com/embed/ii6B9aYJZHk" title="YouTube video player"
# frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
# allowfullscreen></iframe>
# \endhtmlonly
#
# Neil's platform is a fair bit better at balancing the ball, and keeps steady while the ball is placed. It then moves
# the ball to a spot close to the center, though it does not get to the exact center.
#
#
# Craig could not demo the project on his own hardware, as he accidentally broke his board (see \ref brd_iss). This is
# the last recording of his still working platform:
#
# \htmlonly
# <iframe width="600" height="297" src="https://www.youtube.com/embed/FQxBvJwhmho" title="YouTube video player"
# frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
# allowfullscreen></iframe>
# \endhtmlonly
#
# While Craig did not have enough time to fully finish tuning his program, the platform clearly reacts to the touch and
# slowly moves back to equilibrium, though it somewhat overreacts due to its not optimal constant values for the
# controller.
#
# @section dta Platform Data
# Sample data taken during this process can be found at
# https://bitbucket.org/cbelshe/me405_craigbelshe/src/master/TermProject/Data/?at=master.
# Each .csv file has counts in the first column. To get to time, multiply by the period of Controller, or 15ms per entry. The
# second column includes the actual measurements. Data was not all from the same run. Data naturally varies from test to test, depending on ball placement,
# so this data does not reflect what would occur every time.
# The data of x and y positions are plotted below:
#
# \htmlonly
# <iframe src = "x_data.png" width = "1146" height = "621" ></iframe>
#
# \endhtmlonly
#
# The X position of the ball started a little over halfway to the edge of the touch panel, at -40 mm. From there it is
# quickly balance to close to 0 mm, before over correction causes it to roll a bit further away. At time 1275 ms, the
# touch panel seems to have a small error and defaults values to 0. Luckily this is quickly corrected and the ball
# continues moving in the right direction.
#
#
# \htmlonly
# <iframe src = "y_data.png" width = "1036" height = "616" ></iframe>
# \endhtmlonly
#
# Similar to the X data, the Touch panel gives a couple incorrect data measurements for the Y position around 1550 ms in. The 50mm
# measurement was probably caused by detecting the ball, but having the ball lose contact with the panel before sensing
# its location. As a result, it detected the ball in the furthest position. For the most part the ball started fairly
# close to the x axis here, so little correction was needed.
#
# @section err Major Issues
# A fair few problems cropped up, especially when putting everything together.
#
# @subsection tp_iss Touch Panel Issues
# The touch panel had a few problems. The most concerning was a flaky connection along one of the wires connecting it
# to the microcontroller board. This resulted in the panel occasionally losing connection and giving completely false data
# until the wire was readjusted. This error came and went, so was left unresolved since the panel still worked fine
# \b most of the time.
#
# Another major difficulty was the inaccuracy of the touch panel. The readings at either border were not always
# consistent - while measurements were fairly accurate nearer to the center of the panel (after calibration at least),
# the non-linear resistance of the device made measurements less accurate the further out you went. The border was also
# not exactly at the edge of the panel - touching the panel right on the corner gave nonsensical data. This problem
# gave problems for the controller, since it relied on accurate data to decide on a torque for the dc motors. Putting
# the ball too far from the platform made the measurements too inaccurate to work.
#
# One last issue of note, which is fairly amusing looking back, was when a very small sticker somehow attached itself
# to the side of the panel. This resulted in the panel identifying a touch on that edge, and many, many hours were spent trying to
# determine the cause. This issue disappeared when the sticker was noticed and removed.
#
# @subsection mot_iss Motor Issues
# The Motors worked fairly well, and PWM was relatively easy to implement. The biggest problem stemmed from the two
# motors having different torques at the same power. One motor was significantly weaker than the other. This was likely
# just due to a manufacturing difference, since they are the same model of motor. However, it created problems for the
# controller, as it needed to be adjusted so that one motor wasn't running much harder than the other.
#
# @subsection enc_iss Encoder Issues
# The encoders worked quite well from the start, however, the original implementation only handled overflow and not
# underflow. This created some odd data before the issue was resolved. There was also a question of an issue with
# accuracy as the belt tying the motor and the encoder would sometimes slip, but this issue seemed negligible.
#
# @subsection sche_iss Scheduler Issues
# The scheduler was easily the hardest part to get working, and as this is written, is still not fully operationsal - it
# still lacks a proper UI and data keeping. It does manage all the tasks well and they all run quickly and at the
# appropriate time. There were some issues with using too small of a period for the tasks, which caused them to be
# unable to complete on time. This was resolved by increasing the time each task would run for.
#
# The biggest problem with the scheduler was trying to get the correct K constant values. No matter how they were
# adjusted, they didn't seem to work.
#
# @subsection brd_iss Board Issues
# On Craig's board, a screw came loose and fell onto the mcu board. This caused a short between two pins - it even gave
# off a slight spark - and the board stopped working. Even replacing the Nucleo board with a new one did not resolve
# the issue, and the voltage at various pins was incorrect - so some damage must have been done to other components
# as well. This prevented Craig from getting the project running on his own hardware.
#
#
# @author              Craig Belshe
# @author              Neil Patel
# @date                June 11, 2021
##
