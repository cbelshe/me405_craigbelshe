# -*- coding: utf-8 -*-
"""
@file       plot.py
@brief      A basic program to plot the temperature saved indatafile.csv vs time
@details    Reads a file called datafile.csv in the same folder, which 
            contains comma separated values for time, ambient temperature 
            in Celsius, and internal board temperature. Uses the data to 
            plot the temperatures together on the same plot against time.

@author: Craig Belshe
@date May 13, 2021
"""

from matplotlib import pyplot
import array

# Create arrays to store data
time = array.array('f')
temp_amb = array.array('f')
temp_int = array.array('f')

# Get data from datafile.csv
with open('datafile.csv', 'r') as file:
    for line in file:
        line = file.readline()
        [t,temp, tempi] = line.strip().split(',')
        time.append(float(t)//1000)
        temp_amb.append(float(temp))
        temp_int.append(float(tempi))
        
# Plot data
pyplot.figure()
pyplot.plot(time, temp_amb, label='Ambient')
pyplot.plot(time, temp_int, label='Internal')
pyplot.legend()
pyplot.xlabel('Time (S)')
pyplot.ylabel('Temperature, (C)')

