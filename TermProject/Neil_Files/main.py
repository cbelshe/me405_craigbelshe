""" @file main.py
    @brief A scheduler to manage the tasks needed to balance the ball.
    @package Lab0xFF
    @brief This package contains touchPanel.py, motor.py, encoder.py, main.py, controller.py
    @author Group2 2: Neil Patel, Craig Belshe
    @date June 11, 2021
"""

from touchPanel import TouchDriver
from motor import MotorDriver as DRV8847
from controller import Ctrl
from encoder import EncoderDriver as encoder
import task_share
import cotask
import gc
import pyb

## @brief List containing the x-axis positions
x_vals = []
## @brief List containing the x-axis angles
theta_x_vals = []
## @brief List containing the x-axis velocities
x_dot_vals = []
## @brief List containing the x-axis angular velocities
theta_x_dot_vals = []
## @brief List containing the y-axis positions
y_vals = []
## @brief List containing the y-axis angles
theta_y_vals = []
## @brief List containing the y-axis velocities
y_dot_vals = []
## @brief List containing the y-axis angular velocities
theta_y_dot_vals = []

def encode_task():
    '''
    @brief A task to update the angle of the motors.
    @details Initializes the encoders in the Encoder Object.
             Updates the encoders and gets the positions of the encoders in a
             tuple. Uses those values to determine angles and angular velocities.
    @return Does not return anything. Yields the state at the end so that
    the appropriate state is operated in the next iteration.
    '''
    ## @brief Creates the Encoder object.
    enc = encoder()
    
    while True:
        enc.update()
        pos = enc.get_position()
        delta = enc.get_delta()
        queue_ang.put(pos[0])
        queue_ang.put(pos[1])
        queue_vang.put(delta[0] / .04)
        queue_vang.put(delta[1] / .04)
        yield()


def motor_task():
    '''
    @brief A task to run the motors at the correct duty cycle.
    @details Initializes both motors in the Motor object. Checks if any of the
             motors are in the task queue. If they are, then gets the duty cycle
             and sets the direction and power to abalnce the ball. If motor is
             disabled, then clears the fault.
    @return Does not return anything. Yields the state at the end so that
    the appropriate state is operated in the next iteration.
    '''
    ## @brief Creates the Motor object.
    mot = DRV8847()
    mot.enable()
    mot.clear_fault()
    ## @brief Initialize Motor 1 direction
    direction = True
    ## @brief Initialize Motor 2 direction
    direction2 = True
    ## @brief Initialize Motor 1 duty cycle
    D = 0
    ## @brief Initialize Motor 2 duty cycle
    D2 = 0

    while True:
        if queue_motor1.any():
            # Get dutyccycle
            duty = queue_motor1.get()

            # Check Direction
            if duty < 0:
                D = -1*duty
                direction = True
            else:
                D = duty
                direction = False
                
        if queue_motor2.any():
            duty2 = queue_motor2.get()
            if duty2 < 0:
                D2 = -1*duty2
                direction2 = True
            else:
                D2 = duty2
                direction2 = False

        # Run the motor
        if mot.enabled:
            #pass
            mot.set_level_1(direction, D)
            mot.set_level_2(direction2, D2)
        else:
            # Clear fault if motor is disabled
            mot.clear_fault()
            print("fault cleared")
        
        yield()


def touch_task():
    '''
    @brief A task to check the balls position on the touch screen.
    @details Initializes the Touch Panel. Gets the data from the touch panel,
             puts the data into the task queue to measure the positions and
             velocities.
    @return Does not return anything. Yields the state at the end so that
    the appropriate state is operated in the next iteration.
    '''
    ## @brief Creates the Touch Panel object.
    touch = TouchDriver()
    
    while True:
        data = touch.scan_ALL()
        if data[0]:
            queue_x.put(data[1])
            queue_y.put(data[2])
            print("touchpanel:", data)

        else:
            queue_x.put(0)
            queue_y.put(0)

        yield()


def ctrl_task():
    '''
    A task to determine the needed Duty cycle according to the sensor data
    @details Initializes the Controller objects. Using the previous and current
             x and y values from the Touch Panel, calculates the velocities and
             angular velocities. Uses those to calculate the Duty Cycles to input
             into the motors.
    @return Does not return anything. Yields the state at the end so that
    the appropriate state is operated in the next iteration.
    '''
    #ctrl1 = Ctrl(k1=-.15, k2=-0.4, k3=-0.004, k4=-.1)
    #ctrl2 = Ctrl(k1=-.15, k2=-1.8, k3=-0.004, k4=-.1)

    # ctrl1 = Ctrl(k1=-.15, k2=-0.7, k3=-0.004, k4=-.15) # These work but have a settling time > 1 minute
    # ctrl2 = Ctrl(k1=-.15, k2=-2.1, k3=-0.004, k4=-.15)

    # ctrl1 = Ctrl(k1=-.15, k2=-1.7, k3=-0.004, k4=-.15)
    # ctrl2 = Ctrl(k1=-.15, k2=-2.5, k3=-0.004, k4=-.15)

    # ctrl1 = Ctrl(k1=-.15, k2=-1.5, k3=-0.004, k4=-.08)
    # ctrl2 = Ctrl(k1=-.15, k2=-2.4, k3=-0.004, k4=-.08)

    # ctrl1 = Ctrl(k1=-.15, k2=-1.2, k3=-0.004, k4=-.07)
    # ctrl2 = Ctrl(k1=-.15, k2=-2.2, k3=-0.004, k4=-.07)

    # ctrl1 = Ctrl(k1=-.15, k2=-0.9, k3=-0.004, k4=-.11)  # Pretty okay
    # ctrl2 = Ctrl(k1=-.15, k2=-1.4, k3=-0.004, k4=-.11)

    ## @brief Creates the Controller object for Motor 1.
    ctrl1 = Ctrl(k1=-.15, k2=-1.6, k3=-0.004, k4=-.04) # Works well-ish
    ## @brief Creates the Controller object for Motor 2.
    ctrl2 = Ctrl(k1=-.15, k2=-2.3, k3=-0.004, k4=-.04)

    # ctrl1 = Ctrl(k1=-.15, k2=-1.1, k3=-0.004, k4=-.08)
    # ctrl2 = Ctrl(k1=-.15, k2=-1.3, k3=-0.004, k4=-.08)

    #ctrl1 = Ctrl(k1=-.15, k2=-1.4, k3=-0.004, k4=-.04)
    #ctrl2 = Ctrl(k1=-.15, k2=-1.9, k3=-0.004, k4=-.04)

    # ctrl1 = Ctrl(k1=-.15, k2=-1.8, k3=-0.004, k4=-.08)
    # ctrl2 = Ctrl(k1=-.15, k2=-1.8, k3=-0.004, k4=-.08)

    # ctrl1 = Ctrl(k1=-.15, k2=-12, k3=-0.004, k4=-0.5)
    # ctrl2 = Ctrl(k1=-.15, k2=-12.3, k3=-0.004, k4=-0.5)

    #ctrl1 = Ctrl(k1=-.15, k2=-5.5, k3=-0.004, k4=-.15)
    #ctrl2 = Ctrl(k1=-.15, k2=-5.5, k3=-0.004, k4=-.15)

    ## @brief Keeps track of previous x value.
    x_last = 0
    ## @brief Keeps track of x value.
    x = 0
    ## @brief Keeps track of y value.
    y = 0
    ## @brief Keeps track of previous y value.
    y_last = 0
    ## @brief Keeps track of x dot (velocity).
    x_dot = 0
    ## @brief Keeps track of y dot (velocity).
    y_dot = 0
    ## @brief Keeps track of theta y (angle).
    theta_y = 0
    ## @brief Keeps track of theta x (angle).
    theta_x = 0
    ## @brief Keeps track of theta y dot (angular velocity).
    theta_y_dot = 0
    ## @brief Keeps track of theta x dot (angular velocity).
    theta_x_dot = 0
    
    while True:
        # Get X, Y data from the queue
        if queue_x.any():
            x_last = x
            x = queue_x.get() / 1000
            x_dot = (x - x_last) / .04
            
        if queue_y.any():
            y_last = y
            y = queue_y.get() / 1000
            y_dot = (y - y_last) / .04

        # Get theta_x, theta_y data from the queue
        if queue_ang.any():
            theta_x = queue_ang.get() / 4000
            if queue_ang.any():
                theta_y = queue_ang.get() / 4000
        
        if queue_vang.any():
            theta_x_dot = queue_vang.get() / 4000
            if queue_vang.any():
                theta_y_dot = queue_vang.get() / 4000

        # if queue_end.any():
        #     print('ball is off the board')
        #     q = queue_end.get()

        #else:
        # Find the duty cycle for each motor
        #print('ctrl data:', x, theta_x, x_dot, theta_x_dot)
        x_vals.append(x*1000)
        theta_x_vals.append(theta_x*4000)
        x_dot_vals.append(x_dot*1000)
        theta_x_dot_vals.append(theta_x_dot*4000)
        D1 = ctrl1.DutyCycle(x, theta_x, x_dot, theta_x_dot)
        #print('ctrl data:', y, theta_y, y_dot, theta_y_dot)
        y_vals.append(y*1000)
        theta_y_vals.append(theta_y*4000)
        y_dot_vals.append(y_dot*1000)
        theta_y_dot_vals.append(theta_y_dot*4000)
        D2 = ctrl2.DutyCycle(y, theta_y, y_dot, theta_y_dot)

        #print('duty:', D1, D2)
        queue_motor1.put(D1)
        queue_motor2.put(D2)
        yield()      
        
def data_task():
    '''
    @brief A task responsible for collecting data and storing it in appropriate format.
    @details Creates a CSV file and writes into it line-by-line. The line can be
             formatted to display however many values, but the number of values
             displayed is directly proportional to memory consumption. So if more
             values are displayed per line, then the total number of test values
             will decrease.
    @return Does not return anything.
    '''
    # Writes the captured data into a CSV file
    # We created separate CSV files to capture different values as that way
    # we could capture more values without overflowing the memory.
    file1 = open('yDotData.csv', 'w')
    for n in range(len(x_vals)):
        line = "%f, %f\n" % (n, 
                                                         #x_vals[n], 
                                                         #theta_x_vals[n], 
                                                         #x_dot_vals[n],
                                                         #theta_x_dot_vals[n], 
                                                         #y_vals[n], 
                                                         #theta_y_vals[n], 
                                                         y_dot_vals[n],
                                                         #theta_y_dot_vals[n]
                                                         )
        file1.write(line)
    file1.close()
    
def UI_task():
    '''
    @brief A task responsible for interaction between the user and the program.
    @details Runs the schedular with the chosen scheduling algorithm as soon as
             a character is sent through the serial port. Quits if another
             character is sent.
    @return Does not return anything.
    '''
    # Run the scheduler with the chosen scheduling algorithm as soon as a character
    # is sent through the serial port.
    vcp = pyb.USB_VCP()    
    while not vcp.any():
        continue
    
    # Quit if any character is sent through the serial port.
    while not vcp.any():
        cotask.task_list.pri_sched()

    # Empty the comm port buffer of the character(s) just pressed
    vcp.read ()
        
if __name__ == '__main__':

    ## Initializes the Queues and Task to run the Scheduler.
    ## Create Queues to share data between each task.
    queue_x = task_share.Queue ('f', 1, thread_protect = False, overwrite = True,
                           name = "Queue_0")
    queue_y = task_share.Queue ('f', 1, thread_protect = False, overwrite = True,
                           name = "Queue_1")
    queue_ang = task_share.Queue ('f', 2, thread_protect = False, overwrite = True,
                           name = "Queue_2")
    queue_vang = task_share.Queue ('f', 2, thread_protect = False, overwrite = True,
                           name = "Queue_3")
    queue_motor1 = task_share.Queue ('f', 1, thread_protect = False, overwrite = True,
                           name = "Queue_4")
    queue_motor2 = task_share.Queue ('f', 1, thread_protect = False, overwrite = True,
                           name = "Queue_5")
    queue_end = task_share.Queue ('f', 1, thread_protect = False, overwrite = True,
                           name = "Queue_6")

    ## Instantiate each task.
    task_enc = cotask.Task (encode_task, name = 'Task_enc', priority = 3,
                         period = 40, profile = True, trace = False)
    task_mot = cotask.Task (motor_task, name = 'Task_mot', priority = 2,
                         period = 15, profile = True, trace = False)
    task_tp = cotask.Task (touch_task, name = 'Task_tp', priority = 3,
                         period = 40, profile = True, trace = False)
    task_ctrl = cotask.Task (ctrl_task, name = 'Task_ctrl', priority = 2,
                         period = 15, profile = True, trace = False)
    
    ## Create the task list
    cotask.task_list.append(task_enc)
    cotask.task_list.append(task_mot)
    cotask.task_list.append(task_tp)
    cotask.task_list.append(task_ctrl)
    
    ## Run the memory garbage collector to ensure memory is as defragmented as
    ## possible before the real-time scheduler is started
    gc.collect ()

    debug_count = 0
    while debug_count != 2000:
        cotask.task_list.pri_sched()
        debug_count += 1

# Unfortunately, we couldn't get the UI to work in time the way we wanted so we commented it
# out so that functionality remains. The program starts as soon as you run it as opposed
# to how we wanted it to run on a key press.
#    UI_task()
    debug_count = 0
    while debug_count != 2000:
        cotask.task_list.pri_sched()
        debug_count += 1

    data_task()

    print ('\n' + str(cotask.task_list) + '\n')
    print (task_share.show_all ())
#    print (task1.get_trace ())
    print ('\r\n')