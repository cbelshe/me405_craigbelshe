""" @file Encoder.py
    @brief A class driver for using the encoders to measure the encoder angle.
    @author Group 2: Neil Patel, Craig Belshe
    @date June 11, 2021

    @package TermProject
    @brief This package contains touchPanel.py, motor.py, encoder.py, main.py, controller.py
    @author Group 2: Neil Patel, Craig Belshe
    @date June 11, 2021
"""

import pyb
from time import sleep


class EncoderDriver:
    '''
    @brief      A class to interact with two encoders
    @details    This class connects with two encoders and can be used to update their current angle periodically. It can
                also get changes in angle, and reset the current angle to a user defined value.
    '''

    def __init__(self, e1_1=pyb.Pin.board.PB6, e1_2=pyb.Pin.board.PB7,
                 e2_1=pyb.Pin.board.PC6, e2_2=pyb.Pin.board.PC7,
                 current_position_a=0, current_position_b=0):
        '''
        @brief      Initializes the Encoders and their respective timers
        @details
        @param e1_1                 pyb.Pin for Encoder 1, Channel 1 (default=pyb.Pin.board.PB6)
        @param e1_2                 pyb.Pin for Encoder 1, Channel 2 (default=pyb.Pin.board.PB7)
        @param e2_1                 pyb.Pin for Encoder 2, Channel 1 (default=pyb.Pin.board.PC6)
        @param e2_2                 pyb.Pin for Encoder 2, Channel 2 (default=pyb.Pin.board.PC7)
        @param current_position_a   Initial position of Encoder 1 (default=0)
        @param current_position_b   Initial position of Encoder 2 (default=0)
        @return Returns nothing.
        '''

        self.p11 = e1_1
        self.p12 = e1_2
        self.p21 = e2_1
        self.p22 = e2_2

        self.tim4 = pyb.Timer(4)
        self.tim4.init(prescaler=0, period=65535, mode=pyb.Timer.UP)
        self.ch1a = self.tim4.channel(1, mode=pyb.Timer.ENC_AB, pin=self.p11)
        self.ch2a = self.tim4.channel(2, mode=pyb.Timer.ENC_AB, pin=self.p12)

        self.tim8 = pyb.Timer(8)
        self.tim8.init(prescaler=1, period=65535)
        self.ch1b = self.tim8.channel(1, mode=pyb.Timer.ENC_AB, pin=self.p21)
        self.ch2b = self.tim8.channel(2, mode=pyb.Timer.ENC_AB, pin=self.p22)

        self.cnt_a = self.tim4.counter()
        self.cnt_last_a = self.cnt_a
        self.cnt_b = self.tim8.counter()
        self.cnt_last_b = self.cnt_b

        self.pos_a = current_position_a
        self.pos_last_a = current_position_a
        self.pos_b = current_position_b
        self.pos_last_b = current_position_b

    def update(self):
        '''
        @brief Updates the current position of each encoder
        @return Returns nothing.
        '''

        self.cnt_last_a = self.cnt_a
        self.cnt_a = self.tim4.counter()

        self.cnt_last_b = self.cnt_b
        self.cnt_b = self.tim8.counter()

        delta_a = self.cnt_a - self.cnt_last_a
        delta_b = self.cnt_b - self.cnt_last_b

        if delta_a * 2 > 65535:
            delta_a = delta_a - 65535
        elif delta_a * 2 < -65535:
            delta_a = delta_a + 65535

        if delta_b * 2 > 65535:
            delta_b = delta_b - 65535
        elif delta_b * 2 < -65535:
            delta_b = delta_b + 65535

        self.pos_last_a = self.pos_a
        self.pos_last_b = self.pos_b
        self.pos_a = self.pos_a + delta_a
        self.pos_b = self.pos_b + delta_b

        # print('delta', delta_a, delta_b)

    def get_position(self):
        '''
        @brief  Returns the most recently updated positions of the encoders.
        @return Returns a tuple containing the positions of the encoders.
        '''
        return (self.pos_a, self.pos_b)

    def set_position(self, a, b):
        '''
        @brief  Sets the position of the encoders to a specific value.
        @param a    The position for encoder 1
        @param b    The position for encoder 2
        @return Returns nothing.
        '''
        self.pos_a = a
        self.pos_last_a = a

        self.pos_b = b
        self.pos_last_b = b

    def get_delta(self):
        '''
        @brief Calculates difference between the two most recent recorded positions.
        @return Returns a tuple containing the change in position for each encoder between the last two updates.
        '''
        return (self.pos_a - self.pos_last_a, self.pos_b - self.pos_last_b)


if __name__ == '__main__':
    # Testing for the encoders.
    enc = EncoderDriver()
    try:
        while True:
            enc.update()
            print(enc.pos_a, enc.pos_b)
            sleep(1)

    except KeyboardInterrupt:
        print('exiting')