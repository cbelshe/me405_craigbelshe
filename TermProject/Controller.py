# -*- coding: utf-8 -*-
"""
@file Controller.py

@author: Craig Belshe
@date June 1, 2021
"""


class Ctrl:
    def __init__(self, k1, k2, k3, k4):
        '''
        @brief  Initializes the Controller based on calculated gain values
        @param k1, k2, k3, k4   The constants calculated for the system model
        '''

        self.k1 = k1
        self.k2 = k2
        self.k3 = k3
        self.k4 = k4
        
    def DutyCycle(self, pos, ang, vel, ang_vel):
        '''
        @brief          Determines the correct duty cycle for the motor to follow the system model
        @param pos      The position of the ball according to the touch panel measurement
        @param ang      The angle of the ball according to the encoder measurement
        @param vel      The velocity / change in position of the ball
        @param ang_vel  The angular velocity / change in angle of the ball
        '''

        # Calculate Tx = -k*x
        T = self.k1*pos + self.k2*ang + self.k3*vel + self.k4*ang_vel
        R = 2.21    # 2.21 Ohms DC Resistance
        KT = 0.0138 # 13.8 mNm/A Torque Constant
        Vdc = 12    # 12 V DC
        
        # Return the calculated Dutycycle for the appropriate Torque
        return 100*(R/(4*KT*Vdc))*T

