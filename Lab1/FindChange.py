'''
@file           FindChange.py
@brief          Allows for determination of change denominations for a purchase
@author         Craig Belshe
@date           April 21, 2021       
'''


def main():
    '''
    @brief      Tests the getChange function
    @details    Runs two tests for getChange function
    '''
    # test 1
   
    payment = 562
    price = 500
    ans = getChange(price, payment)
    print(ans)  # answer should add to 62
    
    # test 2
    payment = 562
    price = 0
    ans = getChange(price, payment)
    print(ans)  # answer should add to 62




def getChange(price, total):
    '''
    @brief          Finds the change given a price and a payment total
    @details        subtracts price from payment, then returns the exact change
                    needed in a dictionary using twenties, tens, fives, ones, 
                    quarters, dimes, nickels, and pennies
    @param price    INT: The price of the item in cents
    @param total    INT: The total payment in cents
    @return         Returns Dictionary if price <= total. Dictionary is empty if
                    price==total. Otherwise, Dictionary contains a key 
                    corresponding to each denomination with number of that 
                    denomination needed for the change. Returns None if the 
                    price is greater than the total payment
    
    '''
    # Price = int, price of the item in cents
    # payment = Dict, containing number of pennies, nickels, dimes ... twenties (no half-dollar)
    # Output = Dict, change
    # output empty {} if no change, returns None if payment < price

    if price > total:
        # raise ValueError("Price should be less than payment")
        return None
    elif price == total:
        return {}
    else:
        change = total - price

    # Determines output denominations
    output = {}
    denom = [2000, 1000, 500, 100, 25, 10, 5, 1]
    keys = ['twenty', 'ten', 'five', 'one', 'quarter', 'dime', 'nickel', 'penny']
    c = 0
    for key in keys:
        num = change // denom[c]
        output[key] = num
        change -= num*denom[c]
        c += 1

    return output


if __name__ == "__main__":
    main()
