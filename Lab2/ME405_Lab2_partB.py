# -*- coding: utf-8 -*-
"""
@file       ME405_Lab2_partB.py
@brief      A reaction game using timer 2 and button 1
@details    This file creates a game where the user must press the 
            button 1 (blue button) on the STM32L476 Nucleo-64 board
            when an LED lights up on the board. The game determines
            the time it took the user to press the button. Upon exit,
            the average time is displayed. Time measurement is done with
            2 channels on Timer 2: one channel is in output compare (OC)
            mode to start the game every so often, while the other is in
            Input Capture (IC) mode for capturing the time the button is
            pressed. 
            
@author     Craig Belshe
@date       May 2, 2021

"""

import pyb, random

game_on = False
time_start = 0
start = False

# Preallocate 100 spaces for scores to avoid memError
scores = 100*[None]
cnt = 0

def ch1_isr(time):
    '''
    @brief      An interrupt triggered by the channel 1 Timer
    @details    Toggles the LED each time called. Resets its own
                callback after setting a new time so that the LED remains
                on for 1 s, or remains off after the game for 2-3 s. Sets
                the channel 2 timer callback so that a score can be recorded
                after turning the LED on. 
    '''
    global game_on
    global time_start

    # When LED is toggled ON by the callback, start game and set ch1
    # so that the LED is toggled off in 10,000 cnts (or 1 s)
    # else, wait 2-3 s before toggling again
    if game_on:
        #print('LED on')
        ch2.callback(ch2_isr)
        if (time.counter()+10000) > 65536:
            ch1.compare(time.counter()-55536)
        else:
            ch1.compare(time.counter() + 10000)
        time_start = time.counter()
        game_on = False
        
    else:
        num = random.randrange(20000,30000)
        #print('LED off', num)
        if (time.counter()+num) > 65536:
            ch1.compare(time.counter()+num-65536)
        else:
            ch1.compare(time.counter() + num)
        game_on = True
        
    # Reset callback
    ch1.callback(ch1_isr)

def ch2_isr(time):
    '''
    @brief      An interrupt triggered by a button press
    @details    Records the timer counter at the button press
                and compares it to when the LED was turned on
                (by the ch1 isr) to get the the reaction time.
    '''

    global game_on
    global time_start
    global cnt
    
    # Get time of button press from timer
    times = ch2.capture()
    # Turn off ch2 callback until ch1 turns it back on
    ch2.callback(None)
    
    # Record time, taking timer overflow into account
    if times > time_start:
        print('Your score is:', ((times - time_start)//10), 'ms')
        scores[cnt] = (times - time_start)
        cnt += 1
    else:
        print('Your Score is:', ((times + ((65536-time_start)))//10), 'ms')
        scores[cnt] = (times + (65536 - time_start))
        cnt += 1
    
# Initialize Timer, Channels
timer2 = pyb.Timer(2)
#Timer2 w/ ps = 8000, T=65536 -> dt=100 us, total T = 6.55 s
timer2.init(prescaler=8000,period=65536) 
# Ch1: toggle led (PA5) on oc
ch1 = timer2.channel(1, pyb.Timer.OC_TOGGLE, pin=pyb.Pin.board.PA5)
# Ch2: Capture on button press
ch2 = timer2.channel(2, pyb.Timer.IC, pin=pyb.Pin.board.PB3)
ch1.compare(30000)
LED1 = pyb.LED(1)
LED1.toggle()

try:
    while True:
        
        # Initialize game, if initialized then just repeat
        if start == False:
            start = True
            print('Instructions: Press the [Blue] Button when the green' \
                  ' LED lights up to test your reaction time!')
            game_on = True
            limit = True
            ch1.callback(ch1_isr)
            #ch2.callback(ch2_isr)
        
        # To prevent memory errors, limit number of rounds
        if cnt == 100:
            print('You have reached the max number of attempts')
            raise KeyboardInterrupt

# display avg score on exit
except KeyboardInterrupt:
    if cnt == 0:
        print('You did not record any scores.')
        
    else:
        average = 0
        for item in scores:
            if item is not None:
                average += item
        average = average // (cnt)
        print('Your average score over {cnt} tries is' \
              ' {avg} ms'.format(cnt=(cnt), avg=(average/10)))       
    
    print('Thanks for playing! \nGood-bye')
    
