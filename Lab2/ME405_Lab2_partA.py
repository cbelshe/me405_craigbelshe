# -*- coding: utf-8 -*-
"""
@file       ME405_Lab2_partA.py
@brief      A reaction game using the B1 Button and utime
@details    Creates a game where the user must press a button
            when a green LED lights up in order to test their
            reaction time. The time taken to press the button
            is measured inside a callback function called by the
            button pressed, with time being measured first when
            the LED is lit up and second at the button press to
            find the total time.

@author Craig Belshe
@date May 2, 2021

"""

import pyb, utime, random

time_diff = 0
time_start = 0
button_pressed = False
game_on = False

# Preallocate 100 spots for scores
scores = 100*[None]
cnt = 0

def fun():
    '''
    @brief      Function to run when button is pressed
    @details    Using global variables, records the time taken each time
                the button is pressed after the LED illuminates, then turns
                the LED off.

    '''
    global time_diff
    global time_start
    global button_pressed
    global game_on
    global scores
    global cnt
    
    time = utime.ticks_us()
    time_diff = utime.ticks_diff(time, time_start)
    
    if game_on == True:
        print("You took:", time_diff, " MicroSeconds")
        #scores.append(int(time_diff))
        scores[cnt] = time_diff
        cnt += 1
        #print(scores)
      
    #print(scores)
    button_pressed = True
    game_on = False
    LED1.off()
    

# Initialize LED and Switch
LED1 = pyb.LED(1)
SwitchA = pyb.Switch()


try: 
    while True:    
        pyb.delay(random.randrange(2000,3000))
        LED1.on()
        
        SwitchA.callback(fun)
        
        # Begin tracking time
        time_start = utime.ticks_us()
        game_on = True
        
        # Turn LED off automatically after 1 s
        pyb.delay(1000)
        LED1.off()
        
        # Notify user if they did not press the button
        if button_pressed == False:
            print('You did not press the button!!')
            game_on = False
        else:
            button_pressed = False
            
        # To prevent memory errors, limit number of rounds
        if cnt == 100:
            print('You have reached the max number of attempts')
            raise KeyboardInterrupt

# Print average score on exit
except KeyboardInterrupt:
    if cnt == 0:
        print('You did not record any scores')
        
    else:
        average = 0
        for item in scores:
            if item is not None:
                average += item
        average = average // (cnt)
        print('Your average score over {cnt} tries is' \
              ' {avg} us'.format(cnt=(cnt), avg=(average)))       
    
    print('Good-bye')

